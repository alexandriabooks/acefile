import typing
import logging

from .booktypes import AceFileBookParserService

logger = logging.getLogger(__name__)
ALEXANDRIA_PLUGINS: typing.Dict[str, typing.Any] = {}

try:
    ALEXANDRIA_PLUGINS["BookParserService"] = [AceFileBookParserService]
except ImportError as e:
    logger.debug("Not loading acefile plugin", exc_info=e)
