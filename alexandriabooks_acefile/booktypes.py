import typing
import acefile

from alexandriabooks_core import accessors, model
from alexandriabooks_core.services import BookParserService


class AceArchive(accessors.ArchiveBookAccessor):
    def __init__(self, accessor):
        super(AceArchive, self).__init__(accessor)
        self._ace = None

    def open(self):
        self._ace = acefile.open(self.accessor.get_local_path())

    def close(self):
        self._ace.close()

    async def get_file_listing(self):
        return self._ace.getnames()

    async def get_file_stream(self, path):
        return self._ace.read(path)


class AceFileBookParserService(BookParserService):
    def __init__(self, **kwargs):
        pass

    async def can_parse(self, accessor):
        source_type = model.SourceType.find_source_type(accessor.get_filename())
        return source_type in [model.SourceType.CBA]

    async def parse_book(self, accessor):
        pass

    async def enhance_accessor(
        self,
        accessor: model.FileAccessor
    ) -> typing.List[model.FileAccessor]:
        """
        Attempts to find a more specific accessor to aid processing the book.
        :param accessor: The accessor to read the book.
        :return: A list of enhanced accessors.
        """
        if model.SourceType.find_source_type(
            accessor.get_filename()
        ) == model.SourceType.CBA:
            return [AceArchive(accessor)]
        return []

    def __str__(self):
        return "CBA Parser"
